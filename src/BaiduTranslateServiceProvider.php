<?php namespace pilishen\translate;
use Illuminate\Support\ServiceProvider;
class BaiduTranslateServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/baidu-translate.php' => config_path('baidu-translate.php'),
        ]);
    }


    public function register()
    {
        $this->app->bind('pilishen\translate\TranslateApi', 'pilishen\translate\TranslateService');
        $this->app->alias('pilishen\translate\TranslateApi', 'translate');
    }

    public function provides()
    {
        return ['translate'];
    }
}
